package ua.com.radiokot.nburates;

import android.appwidget.AppWidgetManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ua.com.radiokot.nburates.interfaces.RatesLoadingListener;
import ua.com.radiokot.nburates.model.Currency;
import ua.com.radiokot.nburates.tasks.NbuRatesLoadingTask;

public class WidgetConfigActivity extends AppCompatActivity {
	private int widgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
	private static int checkedCount;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_widget_config);

		setResult(RESULT_CANCELED);

		new NbuRatesLoadingTask(new RatesLoadingListener() {
			@Override
			public void onLoadingStarted() {
			}

			@Override
			public void onLoadingFinished(final List<Currency> rates) {
				final boolean[] checked = new boolean[rates.size()];
				showSelectDialog(rates, checked,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int index) {
								Set<String> codes = new HashSet<>();
								for (int i = 0; i < checked.length; i++) {
									if (checked[i]) {
										codes.add(rates.get(i).getCode());
									}
								}

								if (codes.size() > 0) {
									createWidgetAndClose(codes);
								} else {
									finish();
								}
							}
						},
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface, int i) {
								finish();
							}
						});
			}

			@Override
			public void onLoadingFailed(String errorMessage) {
				Toast.makeText(getApplicationContext(),
						R.string.load_rates_failed, Toast.LENGTH_SHORT).show();
				finish();
			}
		}).execute();
	}

	void showSelectDialog(List<Currency> ratesList, final boolean checked[],
						  DialogInterface.OnClickListener okClickListener,
						  DialogInterface.OnClickListener cancelClickListener) {
		String[] currNames = new String[ratesList.size()];
		for (int i = 0; i < ratesList.size(); i++) {
			currNames[i] = ratesList.get(i).getCode() + " ("
					+ ratesList.get(i).getDescription() + ")";
		}

		checkedCount = 0;
		new AlertDialog.Builder(this)
				.setMultiChoiceItems(currNames, checked,
						new DialogInterface.OnMultiChoiceClickListener() {
							@Override
							public void onClick(DialogInterface dialogInterface,
												int i, boolean itemChecked) {
								checkedCount += itemChecked ? 1 : -1;
								checked[i] = itemChecked;

								if (checkedCount > 3) {
									Toast.makeText(getApplicationContext(),
											R.string.toast_too_many_selected,
											Toast.LENGTH_SHORT).show();
									checked[i] = false;
									checkedCount--;
									((AlertDialog) dialogInterface)
											.getListView().setItemChecked(i, false);
								}
							}
						})
				.setPositiveButton(android.R.string.ok, okClickListener)
				.setNegativeButton(android.R.string.cancel, cancelClickListener)
				.setCancelable(false)
				.setTitle(R.string.currencies_select_title)
				.show();
	}

	void createWidgetAndClose(Set<String> codes) {
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
			widgetId = extras.getInt(
					AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
		}

		if (widgetId == AppWidgetManager.INVALID_APPWIDGET_ID) {
			finish();
			return;
		}

		RatesWidgetProvider.setWidgetCurrencies(this, widgetId, codes);

		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
		setResult(RESULT_OK, resultValue);
		finish();

		RatesWidgetProvider.updateWidgets(this,
				AppWidgetManager.getInstance(this), new int[]{widgetId});
	}
}
