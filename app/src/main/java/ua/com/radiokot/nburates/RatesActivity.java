package ua.com.radiokot.nburates;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ua.com.radiokot.nburates.adapters.RatesListAdapter;
import ua.com.radiokot.nburates.interfaces.RatesLoadingListener;
import ua.com.radiokot.nburates.model.Currency;
import ua.com.radiokot.nburates.tasks.NbuRatesLoadingTask;

public class RatesActivity extends AppCompatActivity {
	private RatesListAdapter ratesAdapter;
	private List<Currency> currenciesList = new ArrayList<>();
	private SwipeRefreshLayout swipeRefresh;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_rates);
		Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);

		setUpFab();
		setUpRecyclerView();
		setUpSwipe();
		updateRates();
	}

	void setUpFab() {
		FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
		if (fab == null) {
			return;
		}
		fab.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent convertIntent = new Intent(RatesActivity.this, ConvertActivity.class);
				convertIntent.putExtra("currencies", (ArrayList<Currency>) currenciesList);
				startActivity(convertIntent);
			}
		});
	}

	void setUpRecyclerView() {
		RecyclerView ratesRecyclerView = (RecyclerView) findViewById(R.id.list_rates);
		if (ratesRecyclerView == null) {
			return;
		}
		RecyclerView.LayoutManager ratesLayoutManager = new GridLayoutManager(this, 2);
		ratesRecyclerView.setLayoutManager(ratesLayoutManager);

		ratesAdapter = new RatesListAdapter(this, currenciesList);
		ratesRecyclerView.setAdapter(ratesAdapter);
	}

	void setUpSwipe() {
		swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh);
		swipeRefresh.setProgressViewOffset(false, 0,
				(int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 24, getResources().getDisplayMetrics()));
		swipeRefresh.setColorSchemeResources(
				R.color.colorOrange,
				R.color.colorPrimaryDark,
				R.color.colorLightBlue);
		swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				updateRates();
			}
		});
	}

	void updateRates() {
		new NbuRatesLoadingTask(new RatesLoadingListener() {
			@Override
			public void onLoadingStarted() {
				setShowProgress(true);
			}

			@Override
			public void onLoadingFinished(List<Currency> currencies) {
				RatesActivity.this.currenciesList.clear();
				RatesActivity.this.currenciesList.addAll(currencies);
				ratesAdapter.notifyDataSetChanged();
				setShowProgress(false);
			}

			@Override
			public void onLoadingFailed(String errorMessage) {
				showSnack(R.string.load_rates_failed);
				setShowProgress(false);
			}
		}).execute();
	}

	void setShowProgress(boolean show) {
		swipeRefresh.setRefreshing(show);
	}

	void showSnack(int strId) {
		Snackbar.make(swipeRefresh, strId, Snackbar.LENGTH_SHORT).show();
	}
}
