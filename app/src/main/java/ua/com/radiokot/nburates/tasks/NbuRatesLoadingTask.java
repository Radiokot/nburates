package ua.com.radiokot.nburates.tasks;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import ua.com.radiokot.nburates.interfaces.RatesLoadingListener;
import ua.com.radiokot.nburates.model.Currency;
import ua.com.radiokot.nburates.model.NbuCurrencyFactory;

public class NbuRatesLoadingTask extends AsyncTask<Void, Void, Boolean> {
	private RatesLoadingListener loadingListener;

	private List<Currency> currencies;
	private String errorMessage = null;

	public NbuRatesLoadingTask(RatesLoadingListener loadingListener) {
		this.loadingListener = loadingListener;
	}

	@Override
	protected void onPreExecute() {
		loadingListener.onLoadingStarted();
	}

	@Override
	protected Boolean doInBackground(Void... voids) {
		OkHttpClient httpClient = new OkHttpClient();
		Request nbuApiRequest = new Request.Builder()
				.url("https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json")
				.build();

		try {
			Response nbuApiResponse = httpClient.newCall(nbuApiRequest).execute();
			JSONArray currenciesJson = new JSONArray(nbuApiResponse.body().string());

			List<Currency> priorityCurrencies = new ArrayList<>();
			List<Currency> otherCurrencies = new ArrayList<>();

			for (int i = 0; i < currenciesJson.length(); i++) {
				try {
					JSONObject itemJson = currenciesJson.getJSONObject(i);
					Currency item = NbuCurrencyFactory.fromJson(itemJson);

					// "Долар США по розр.з Iндiєю" with empty code.
					if (item.getCode().trim().equals("")) {
						continue;
					}

					String itemCode = item.getCode();
					if (itemCode.equals("UAH") || itemCode.equals("USD")
							|| itemCode.equals("EUR") || itemCode.equals("RUB")) {
						priorityCurrencies.add(item);
					} else {
						otherCurrencies.add(item);
					}
				} catch (JSONException exception) {
					continue;
				}
			}

			currencies = new ArrayList<>();
			currencies.addAll(priorityCurrencies);
			currencies.addAll(otherCurrencies);

			return true;

		} catch (IOException|JSONException exception) {
			errorMessage = exception.getMessage();
			return false;
		}
	}

	@Override
	protected void onPostExecute(Boolean success) {
		if (!success) {
			loadingListener.onLoadingFailed(this.errorMessage);
		} else {
			loadingListener.onLoadingFinished(currencies);
		}
	}
}
