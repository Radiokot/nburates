package ua.com.radiokot.nburates.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import ua.com.radiokot.nburates.R;
import ua.com.radiokot.nburates.model.Currency;

public class RatesListAdapter extends RecyclerView.Adapter<RatesListAdapter.ViewHolder> {
	class ViewHolder extends RecyclerView.ViewHolder {
		TextView textRate;
		TextView textCode;

		ViewHolder(View view) {
			super(view);
			this.textRate = (TextView) view.findViewById(R.id.text_currency_rate);
			this.textCode = (TextView) view.findViewById(R.id.text_currency_code);
		}
	}

	private List<Currency> currencies;
	private Context context;

	public RatesListAdapter(Context context, List<Currency> currencies) {
		this.currencies = currencies;
		this.context = context;
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.currency_card, parent, false);

		return new ViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		Currency item = currencies.get(position);

		holder.textRate.setText(String.format(
				context.getResources().getConfiguration().locale, "%.3f", item.getRate()));
		holder.textCode.setText(item.getCode());
	}

	@Override
	public int getItemCount() {
		return currencies.size();
	}
}
