package ua.com.radiokot.nburates;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;

import ua.com.radiokot.nburates.model.Currency;

public class ConvertActivity extends AppCompatActivity {
	private ArrayList<Currency> currencies;
	private Currency fromCurrency;
	private Currency toCurrency;
	private EditText editConvertAmount;
	private TextView textConvertFrom;
	private TextView textConvertTo;
	private TextView textConvertResult;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_convert);

		Intent startIntent = getIntent();
		try {
			currencies = (ArrayList<Currency>) startIntent.getSerializableExtra("currencies");
		} catch (ClassCastException exception) {
			currencies = new ArrayList<>();
		}

		setUpSwapButton();
		setUpEditAmount();
		setUpTextViews();

		setConvertFrom(currencies.get(0));
		setConvertTo(currencies.get(1));
		calculate();
	}

	void setUpSwapButton() {
		ImageButton buttonSwap = (ImageButton) findViewById(R.id.button_swap);
		if (buttonSwap == null) {
			return;
		}
		buttonSwap.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				swapCurrencies();
			}
		});
	}

	void setUpEditAmount() {
		editConvertAmount = (EditText) findViewById(R.id.edit_convert_amount);
		if (editConvertAmount == null) {
			return;
		}
		editConvertAmount.setSelection(editConvertAmount.getText().length());
		editConvertAmount.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
				calculate();
			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});
	}

	void setUpTextViews() {
		textConvertFrom = (TextView) findViewById(R.id.text_convert_from);
		textConvertFrom.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						setConvertFrom(currencies.get(i));
						calculate();
					}
				});
			}
		});
		textConvertTo = (TextView) findViewById(R.id.text_convert_to);
		textConvertTo.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showSelectDialog(new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						setConvertTo(currencies.get(i));
						calculate();
					}
				});
			}
		});
		textConvertResult = (TextView) findViewById(R.id.text_convert_result);
	}

	void setConvertFrom(Currency from) {
		fromCurrency = from;
		textConvertFrom.setText(from.getCode());
	}

	void setConvertTo(Currency to) {
		toCurrency = to;
		textConvertTo.setText(to.getCode());
	}

	void swapCurrencies() {
		Currency currencyFrom = fromCurrency;
		setConvertFrom(toCurrency);
		setConvertTo(currencyFrom);
		calculate();
	}

	void calculate() {
		double amount;
		try {
			amount = Double.parseDouble(editConvertAmount.getText().toString());
		} catch (NumberFormatException exception) {
			return;
		}
		double result = toCurrency.fromBase(fromCurrency.toBase(amount));
		textConvertResult.setText(String.format(
				getResources().getConfiguration().locale, "%.3f", result));
	}

	void showSelectDialog(DialogInterface.OnClickListener clickListener) {
		String[] currNames = new String[currencies.size()];
		for (int i = 0; i < currencies.size(); i++) {
			currNames[i] = currencies.get(i).getCode() + " ("
					+ currencies.get(i).getDescription() + ")";
		}
		new AlertDialog.Builder(this)
			.setItems(currNames, clickListener)
			.show();
	}
}
