package ua.com.radiokot.nburates;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import ua.com.radiokot.nburates.interfaces.RatesLoadingListener;
import ua.com.radiokot.nburates.model.Currency;
import ua.com.radiokot.nburates.tasks.NbuRatesLoadingTask;

import static android.content.Context.MODE_PRIVATE;

public class RatesWidgetProvider extends AppWidgetProvider {
	private final static String WIDGETS_PREFS = "RatesWidget";
	private final static String WIDGETS_PREFS_CURRENCIES = "Currencies";

	public void onUpdate(final Context context, final AppWidgetManager widgetManager, final int[] widgetIds) {
		updateWidgets(context, widgetManager, widgetIds);
	}

	@Override
	public void onDeleted(Context context, int[] widgetIds) {
		for (int widgetId : widgetIds) {
			removeWidgetCurrencies(context, widgetId);
		}
	}

	public static void updateWidgets(final Context context,
									 final AppWidgetManager widgetManager, final int[] widgetIds) {
		new NbuRatesLoadingTask(new RatesLoadingListener() {
			@Override
			public void onLoadingStarted() {
			}

			@Override
			public void onLoadingFinished(List<Currency> currencies) {
				for (int widgetId : widgetIds) {
					RemoteViews widgetLayout = new RemoteViews(context.getPackageName(), R.layout.rates_widget_layout);
					widgetLayout.removeAllViews(R.id.layout_root_widget);

					Log.i("Widget", widgetId + " UPDATE");
					Set<String> widgetCurrencies = getWidgetCurrencies(context, widgetId);

					for (Currency currency : currencies) {
						if (!widgetCurrencies.contains(currency.getCode())) {
							continue;
						}
						widgetCurrencies.remove(currency.getCode());

						RemoteViews currencyLayout = new RemoteViews(context.getPackageName(),
								R.layout.currency_card_content);
						currencyLayout.setTextViewText(R.id.text_currency_code,
								currency.getCode());
						currencyLayout.setTextViewText(R.id.text_currency_rate,
								String.format(context.getResources().getConfiguration().locale,
										"%.2f", currency.getRate()));

						widgetLayout.addView(R.id.layout_root_widget, currencyLayout);
					}

					widgetManager.updateAppWidget(widgetId, widgetLayout);
				}
			}

			@Override
			public void onLoadingFailed(String errorMessage) { }
		}).execute();
	}

	public static Set<String> getWidgetCurrencies(Context context, int widgetId) {
		SharedPreferences prefs = context.getSharedPreferences(WIDGETS_PREFS, MODE_PRIVATE);
		Set<String> codes = prefs.getStringSet(WIDGETS_PREFS_CURRENCIES + widgetId,
				new HashSet<String>());

		Log.i("Widget", widgetId + " GET " + codes);
		return codes;
	}

	public static void setWidgetCurrencies(Context context,
										   int widgetId, Set<String> codes) {
		Log.i("Widget", widgetId + " SET " + codes.toString());
		SharedPreferences.Editor prefsEditor =
				context.getSharedPreferences(WIDGETS_PREFS, MODE_PRIVATE).edit();
		prefsEditor.putStringSet(WIDGETS_PREFS_CURRENCIES + widgetId, codes);
		prefsEditor.apply();
	}

	public static void removeWidgetCurrencies(Context context, int widgetId) {
		Log.i("Widget", widgetId + " DELETE");
		SharedPreferences.Editor prefsEditor =
				context.getSharedPreferences(WIDGETS_PREFS, MODE_PRIVATE).edit();
		prefsEditor.remove(WIDGETS_PREFS_CURRENCIES + widgetId);
		prefsEditor.apply();
	}
}
