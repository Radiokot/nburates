package ua.com.radiokot.nburates.interfaces;

import java.util.List;

import ua.com.radiokot.nburates.model.Currency;

public interface RatesLoadingListener {
	void onLoadingStarted();
	void onLoadingFinished(List<Currency> currencies);
	void onLoadingFailed(String errorMessage);
}
