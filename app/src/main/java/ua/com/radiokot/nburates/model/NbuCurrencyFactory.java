package ua.com.radiokot.nburates.model;

import org.json.JSONException;
import org.json.JSONObject;

public class NbuCurrencyFactory {
	public static Currency fromJson(JSONObject currencyJson) throws JSONException {
		return new Currency(
				currencyJson.getString("cc"),
				currencyJson.getString("txt"),
				currencyJson.getDouble("rate"));
	}
}
