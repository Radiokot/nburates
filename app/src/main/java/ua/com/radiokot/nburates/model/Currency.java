package ua.com.radiokot.nburates.model;

import java.io.Serializable;

public class Currency implements Serializable {
	private String code;
	private String description;
	private double rate;

	public Currency(String code, String description, double rate) {
		if (rate <= 0) {
			throw new IllegalArgumentException("Currency rate can't be zero or lower than zero");
		}

		this.code = code;
		this.description = description;
		this.rate = rate;
	}

	public String getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public double getRate() {
		return rate;
	}

	public double toBase(double amount) {
		return amount * rate;
	}

	public double fromBase(double amount) {
		return amount / rate;
	}
}
